<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;

class LocationController extends Controller
{
    public function nearestLocation(Request $request){
    	$request->validate([
    		'latitude' => 'required|numeric',
    		'longitude' => 'required|numeric',
    	]);

    	return  Location::select(\DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
        // ->having('distance', '<', 50) // to get nearest_location on distnation 50 KM
	    ->orderBy('distance')
	    ->paginate(10);
    }
}
