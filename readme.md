# Standard Project for 
## About
Implementation on " laravel ".

## Requirments:
- xampp
- php v 7.*
- composer
- laravel

## Setup / configuration
 1. clone Repo 
    - `git clone https://github.com/anonymous-persone/nearest-location`
    - cd into your project
 2. Install Composer Dependencies
    - ```composer install```
 3. Install NPM Dependencies [OPTIONAL]
    - `npm install`
 4. Create a copy of your .env file
    - ```cp .env.example .env```
 5. Generate an app encryption key
    - ```php artisan key:generate```
    - If the application key is not set, your user sessions and other encrypted data will not be secure!
 6. Create an empty database for our application
 7. In the .env file, add database information to allow Laravel to connect to the database
 8. Migrate the database
    - `php artisan migrate`
 9. [Optional]: Seed the database
    - `php artisan db:seed`

## To Run
1. you could use vhost or run `php artisan serve`

# License 
this project is free to every one who is starting to learn Laravel
> Author : Eslam Ayman 