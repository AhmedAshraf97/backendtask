<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Location;
use Faker\Generator as Faker;

$factory->define(Location::class, function (Faker $faker) {
	// $faker = Faker::create('de_DE'); // to localization
	
    return [
    	'latitude' => $faker->latitude,
    	'longitude' => $faker->longitude,
        'name' => $faker->streetAddress,
    ];
});
